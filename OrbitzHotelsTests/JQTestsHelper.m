//
//  JQTestsHelper.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQTestsHelper.h"

@implementation JQTestsHelper

NSDictionary* (^loadTestFixtureWithName)(NSString*) = ^(NSString* name) {
    NSBundle* testTargetBundle = [NSBundle bundleForClass:NSClassFromString(@"JQHotel_MapperSpec")];
    NSString* filePath = [testTargetBundle pathForResource:name ofType:@"json"];
    NSString* jsonString = [NSString stringWithContentsOfFile:filePath encoding:NSStringEncodingConversionExternalRepresentation error:nil];
    NSData* jsonData = [jsonString dataUsingEncoding:NSStringEncodingConversionExternalRepresentation];
    NSError* error;
    NSDictionary* result = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    return result;
};

@end
