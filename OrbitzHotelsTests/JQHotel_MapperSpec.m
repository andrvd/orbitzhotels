//
//  JQHotel_MapperSpec.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import <Kiwi/Kiwi.h>
#import "JQHotel+Mapper.h"
#import "JQTestsHelper.h"
#import <Mantle/Mantle.h>

SPEC_BEGIN(JQHotel_MapperSpec)

context(@"Conformance to protocol", ^{
    
    __block JQHotel* subject = nil;
    
    beforeEach(^{
        subject = [[JQHotel alloc] init];
    });
   
    it(@"should conform to <MTLJSONSerializing>", ^{
        [[subject should] conformToProtocol:@protocol(MTLJSONSerializing)];
    });
    
    it(@"should implement the mandatory protocol methods", ^{
        [[JQHotel should] respondToSelector:@selector(JSONKeyPathsByPropertyKey)];
    });
});

context(@"Mapping", ^{
    __block JQHotel* subject = nil;
    
    beforeAll(^{
        NSDictionary* jsonDictionary = loadTestFixtureWithName(@"hotels");
        NSArray* hotelsArray = jsonDictionary[@"hotels"];
        NSDictionary* hotelDictionary = [hotelsArray firstObject];
        NSError *error = nil;
        subject = [MTLJSONAdapter modelOfClass:JQHotel.class
                            fromJSONDictionary:hotelDictionary
                                         error:&error];
    });
    
   
    it(@"should map json values to hotel model correctly", ^{
        [[subject.distance shouldNot] beNil];
        [[subject.direction shouldNot] beNil];
        [[subject.starRating shouldNot] beNil];
        [[subject.name shouldNot] beNil];
        [[subject.nightlyRate shouldNot] beNil];
        [[subject.promotedNightlyRate shouldNot] beNil];
        [[subject.totalRate shouldNot] beNil];
        [[subject.longitude shouldNot] beNil];
        [[subject.key shouldNot] beNil];
        [[subject.totalRate shouldNot] beNil];
        [[subject.latitude shouldNot] beNil];
        [[subject.masterId shouldNot] beNil];
        [[subject.thumbnail shouldNot] beNil];
        [[subject.streetAddress shouldNot] beNil];
        [[subject.reviewScore shouldNot] beNil];
    });
    
});

SPEC_END