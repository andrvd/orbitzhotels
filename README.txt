Requirements:
-iOS 8
-Install cocoapods gem
-Run pod update
-Internet connection

Notes:
This app is an excercise to apply for the iOS Developer position at Orbitz, Chicago, IL.
The app covers the minimum requirements, and also replaced the TabBarController implementation with view containers. The view containers approach allows to have a mainViewController with the ability to show 2 viewControllers at the same time: mapsViewController, and searchViewController.
This approach required more time than I expected, but the result is very useful.

In order to run the app, just run the simulator. In the search bar, type the name of a hotel, based on the hotels.json list that was provided.
The search mechanisn will suggest a result, it can be tapped, then this result will be shown in the map.

Thank you for review this.

Jorge Quezada
08/29/2015
