//
//  JQDataManager.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQDataManager.h"
#import "JQHotel.h"
#import "JQUtils.h"
#import <Mantle/Mantle.h>

@interface JQDataManager()
@property (nonatomic, strong) NSArray* cache;
@end

@implementation JQDataManager

- (NSArray*)data
{
    if (self.cache == nil) {
        [self loadCache];
    }
    return self.cache;
}

- (void)loadCache
{
    NSDictionary* fullDictionary = [JQUtils hotelsDictionary];
    NSArray* hotelsArray = fullDictionary[@"hotels"];
    
    __block NSMutableArray* mutableArray = [NSMutableArray array];
    [hotelsArray enumerateObjectsUsingBlock:^(NSDictionary* dictionary, NSUInteger idx, BOOL *stop) {
        
        NSError* error = nil;
        JQHotel* hotel = [MTLJSONAdapter modelOfClass:JQHotel.class
                                   fromJSONDictionary:dictionary
                                                error:&error];
        NSAssert(error == nil, @"Unable to parse hotels.json");
        
        if (hotel) {
            [mutableArray addObject:hotel];
        }
    }];
    
    NSArray* resultArray = [NSArray arrayWithArray:mutableArray];
    self.cache = resultArray;
}

@end
