//
//  JQLocationManager.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/29/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQLocationManager.h"

@interface JQLocationManager()
@property (nonatomic, strong) CLLocationManager* locationManager;
@end

@implementation JQLocationManager

+ (id)sharedInstance {
    static JQLocationManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[self alloc] init];
    });
    return manager;
}

- (void)start
{
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [self.locationManager setDistanceFilter:kCLDistanceFilterNone];
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
}

- (void)setDelegate:(id<CLLocationManagerDelegate>)delegate
{
    self.locationManager.delegate = delegate;
}

- (id<CLLocationManagerDelegate>)delegate
{
    return self.locationManager.delegate;
}

- (CLLocation*)location
{
    return self.locationManager.location;
}

@end
