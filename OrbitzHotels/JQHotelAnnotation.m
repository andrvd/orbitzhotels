//
//  JQHotelAnnotation.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/29/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQHotelAnnotation.h"
#define kAnnotationIdentifier @"hotelAnnotationId"

@interface JQHotelAnnotation()
@property (nonatomic, strong) JQHotelAdapter* hotel;
@end

@implementation JQHotelAnnotation

- (instancetype)initWithHotel:(JQHotelAdapter*)hotel
{
    self = [super init];
    if (self) {
        _hotel = hotel;
    }
    return self;
}

- (CLLocationCoordinate2D)coordinate
{
    return self.hotel.location.coordinate;
}

- (NSString *)title
{
    return self.hotel.name;
}

- (NSString *)subtitle
{
    return self.hotel.streetAddress;
}

- (MKAnnotationView*)annotationView
{
    MKAnnotationView* customAnnotationView = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:kAnnotationIdentifier];
    customAnnotationView.enabled = YES;
    customAnnotationView.canShowCallout = YES;
    customAnnotationView.image = [UIImage imageNamed:@"pin"];
    return customAnnotationView;
}

- (NSString*)annotationIdentifier
{
    return kAnnotationIdentifier;
}
@end
