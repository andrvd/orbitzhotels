//
//  JQContainerView.h
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/29/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface JQContainerView : UIView
@property (nonatomic, assign) BOOL shouldForwardTouches;
@property (nonatomic, strong) UIViewController *containingViewController;
@end
