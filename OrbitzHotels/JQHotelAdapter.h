//
//  JQHotelAdapter.h
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JQHotel.h"
#import <CoreLocation/CoreLocation.h>

@interface JQHotelAdapter : JQHotel

@property (nonatomic) NSInteger index;
@property (nonatomic, strong) UIImage* thumbnailImage;
@property (nonatomic, strong) NSURL* thumbnailURL;

- (NSNumber*)starRatingValue;
- (NSNumber*)distanceValue;
- (NSNumber*)totalRateValue;
- (UIImage *)rateImage;
- (instancetype)initWithHotel:(JQHotel*)hotel;
- (CLLocation*)location;
@end
