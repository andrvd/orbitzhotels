//
//  JQMapViewController.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQMapViewController.h"
#import "JQHotelAnnotation.h"

@interface JQMapViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) JQHotelAnnotation* currentAnnotation;
@end

@implementation JQMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureMapViews];
}

- (void)configureMapViews
{
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
}

- (void)showAnnotationForHotel:(JQHotelAdapter*)hotel
{
    self.currentAnnotation = [[JQHotelAnnotation alloc] initWithHotel:hotel];
    [self.mapView addAnnotation:self.currentAnnotation];
    [self pointToRegionWithLocation:hotel.location];
}

#pragma  mark - MKmapViewADelegate methods

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    [self pointToRegionWithLocation:userLocation.location];
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    if (self.currentAnnotation != nil) {
        [mapView selectAnnotation:self.currentAnnotation animated:YES];
    }
}

- (void)pointToRegionWithLocation:(CLLocation*)location
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location.coordinate, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    JQHotelAnnotation *hotelAnnotation = (JQHotelAnnotation*)annotation;
    
    if(annotation != mapView.userLocation) {
        MKAnnotationView* annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:hotelAnnotation.annotationIdentifier];
        if (annotationView == nil) {
            annotationView = hotelAnnotation.annotationView;
        }
        return annotationView;
    }
   
    return nil;
}

@end