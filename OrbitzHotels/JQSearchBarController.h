//
//  JQSearchBarController.h
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQViewController.h"
#import "JQHotelAdapter.h"
#import "JQMainViewController.h"

typedef void (^JQSearchBarActionBlock)(JQHotelAdapter*);

@interface JQSearchBarController : JQViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) NSArray* hotelAdapters;
- (IBAction)filterButtonTapped:(id)sender;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, copy) JQSearchBarActionBlock actionBlock;
@end
