//
//  JQHotelAnnotation.h
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/29/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "JQHotelAdapter.h"

@interface JQHotelAnnotation : NSObject<MKAnnotation>

- (instancetype)initWithHotel:(JQHotelAdapter*)hotel;
- (MKAnnotationView*)annotationView;
- (NSString*)annotationIdentifier;
@end
