//
//  JQMainViewController.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQMainViewController.h"
#import "JQSearchBarController.h"
#import "JQMapViewController.h"
#import "JQLocationManager.h"
#import "JQContainerView.h"
#import "JQHotelAdapter.h"

#define kSearchBarSegueIdentifier @"showSearchBar"
#define kMapSegueIdentifier @"showMap"

//41.882408, -87.640964
@interface JQMainViewController ()
@property (nonatomic, weak) IBOutlet JQContainerView *mapContainer;
@property (nonatomic, weak) IBOutlet JQContainerView *searchBarContainer;
@property (nonatomic, strong) JQMapViewController* mapViewController;

@end

@implementation JQMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureContainers];
    [self configureLocation];
}

- (void)configureContainers
{
    self.mapContainer.shouldForwardTouches = NO;
    self.searchBarContainer.shouldForwardTouches = YES;
}

- (void)configureLocation
{
    JQLocationManager* locationManager = [JQLocationManager sharedInstance];
    [locationManager start];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    __weak typeof(self) welf = self;
    if ([segue.identifier isEqualToString:kSearchBarSegueIdentifier]) {
        JQSearchBarController* searchBarController = segue.destinationViewController;
        searchBarController.hotelAdapters = self.hotelAdapters;
        searchBarController.actionBlock = ^(JQHotelAdapter* hotel) {
            if (welf.mapViewController) {
                [welf.mapViewController showAnnotationForHotel:hotel];
            }
        };
    } else if ([segue.identifier isEqualToString:kMapSegueIdentifier]) {
        self.mapViewController = segue.destinationViewController;
    }
}

- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}

@end
