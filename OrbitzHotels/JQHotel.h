//
//  JQHotel.h
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface JQHotel : MTLModel
@property (nonatomic, copy) NSString* distance;
@property (nonatomic, copy) NSString* direction;
@property (nonatomic, copy) NSString* starRating;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* nightlyRate;
@property (nonatomic, copy) NSString* promotedNightlyRate;
@property (nonatomic, copy) NSString* totalRate;
@property (nonatomic, strong) NSNumber* longitude;
@property (nonatomic, copy) NSString* key;
@property (nonatomic, copy) NSString* promotedTotalRate;
@property (nonatomic, strong) NSNumber* latitude;
@property (nonatomic, strong) NSNumber* masterId;
@property (nonatomic, copy) NSString* thumbnail;
@property (nonatomic, copy) NSString* streetAddress;
@property (nonatomic, strong) NSNumber* reviewScore;
@end
