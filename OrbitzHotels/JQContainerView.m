//
//  JQContainerView.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/29/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQContainerView.h"

@implementation JQContainerView

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if (self.shouldForwardTouches == NO) {
        return [super pointInside:point withEvent:event];
    }
    
    __block BOOL canHandle = NO;
    
    [self.subviews enumerateObjectsUsingBlock:^(UIView *subView, NSUInteger idx, BOOL *stop) {
        CGPoint convertedPoint = [self convertPoint:point toView:subView];
        canHandle = (subView.alpha > 0
                     && subView.hidden == NO
                     && subView.userInteractionEnabled
                     && [subView pointInside:convertedPoint  withEvent:event]);
        *stop = canHandle;
    }];
    
    return canHandle;
}

@end
