//
//  JQHotelAdapter.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQHotelAdapter.h"
#import "JQHotel.h"

@interface JQHotelAdapter()
@property (nonatomic, copy) NSString* internalDistance;
@property (nonatomic, copy) NSString* internalName;
@property (nonatomic, copy) NSString* internalTotalRate;
@end

@implementation JQHotelAdapter

- (instancetype)initWithHotel:(JQHotel*)hotel
{
    self = [super init];
    if (self) {
        _internalDistance = hotel.distance;
        self.direction = hotel.direction;
        self.starRating = hotel.starRating;
        _internalName = hotel.name;
        self.nightlyRate = hotel.nightlyRate;
        self.promotedNightlyRate = hotel.promotedNightlyRate;
        _internalTotalRate = hotel.totalRate;
        self.longitude = hotel.longitude;
        self.key = hotel.key;
        self.promotedTotalRate = hotel.promotedTotalRate;
        self.latitude = hotel.latitude;
        self.masterId = hotel.masterId;
        self.thumbnail = hotel.thumbnail;
        if (self.thumbnail.length > 0) {
            _thumbnailURL = [NSURL URLWithString:self.thumbnail];
        }
        self.streetAddress = hotel.streetAddress;
        self.reviewScore = hotel.reviewScore;
    }
    return self;
}

- (NSString *)name
{
    return [NSString stringWithFormat:@"%ld.-%@", (long)_index, _internalName];
}

- (UIImage *)thumbnailImage
{
    return _thumbnailImage?:[UIImage imageNamed:@"orbitz"];
}

- (NSNumber*)starRatingValue
{
    double value = [self.starRating doubleValue];
    NSNumber* number = @(value);
    return number;
}

- (UIImage *)rateImage
{
    NSString* imageName = [NSString stringWithFormat:@"s%@", self.starRating];
    return [UIImage imageNamed:imageName];
}

- (NSString *)distance
{
    return [NSString stringWithFormat:@"%@ mi", _internalDistance];
}

- (NSNumber*)distanceValue
{
    double value = [self.distance doubleValue];
    NSNumber* number = @(value);
    return number;
}

- (NSString *)totalRate
{
    return [NSString stringWithFormat:@"$%@", _internalTotalRate];
}

- (NSNumber*)totalRateValue
{
    double value = [_internalTotalRate doubleValue];
    NSNumber* number = @(value);
    return number;
}

- (CLLocation*)location
{
    CLLocation* result = [[CLLocation alloc] initWithLatitude:self.latitude.doubleValue longitude:self.longitude.doubleValue];
    return result;
}
@end
