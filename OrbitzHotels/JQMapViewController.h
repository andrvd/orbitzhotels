//
//  JQMapViewController.h
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "JQHotelAdapter.h"

@interface JQMapViewController : UIViewController <MKMapViewDelegate>
- (void)showAnnotationForHotel:(JQHotelAdapter*)hotel;
@end
