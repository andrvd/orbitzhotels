//
//  JQHotel+Mapper.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQHotel+Mapper.h"

static const NSString * kJQHotelDistancePropertyJSONKeyName = @"distance";
static const NSString * kJQHotelDirectionPropertyJSONKeyName = @"direction";
static const NSString * kJQHotelStarRatingPropertyJSONKeyName = @"star_rating";
static const NSString * kJQHotelNamePropertyJSONKeyName = @"name";
static const NSString * kJQHotelNightlyRatePropertyJSONKeyName = @"nightly_rate";
static const NSString * kJQHotelPromotedNightlyRatePropertyJSONKeyName = @"promoted_nightly_rate";
static const NSString * kJQHotelTotalRatePropertyJSONKeyName = @"total_rate";
static const NSString * kJQHotelLongitudePropertyJSONKeyName = @"longitude";
static const NSString * kJQHotelKeyPropertyJSONKeyName = @"key";
static const NSString * kJQHotelPromotedTotalRatePropertyJSONKeyName = @"promoted_total_rate";
static const NSString * kJQHotelLatitudePropertyJSONKeyName = @"latitude";
static const NSString * kJQHotelMasterIdPropertyJSONKeyName = @"master_id";
static const NSString * kJQHotelThumbnailPropertyJSONKeyName = @"thumbnail";
static const NSString * kJQHotelStreetAddressPropertyJSONKeyName = @"street_address";
static const NSString * kJQHotelReviewScorePropertyJSONKeyName = @"review_score";

@implementation JQHotel (Mapper)

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"distance" : kJQHotelDistancePropertyJSONKeyName,
             @"direction" : kJQHotelDirectionPropertyJSONKeyName,
             @"starRating" : kJQHotelStarRatingPropertyJSONKeyName,
             @"name" : kJQHotelNamePropertyJSONKeyName,
             @"nightlyRate" : kJQHotelNightlyRatePropertyJSONKeyName,
             @"promotedNightlyRate" : kJQHotelPromotedNightlyRatePropertyJSONKeyName,
             @"totalRate" : kJQHotelTotalRatePropertyJSONKeyName,
             @"longitude" : kJQHotelLongitudePropertyJSONKeyName,
             @"key" : kJQHotelKeyPropertyJSONKeyName,
             @"promotedTotalRate" : kJQHotelPromotedTotalRatePropertyJSONKeyName,
             @"latitude" : kJQHotelLatitudePropertyJSONKeyName,
             @"masterId" : kJQHotelMasterIdPropertyJSONKeyName,
             @"thumbnail" : kJQHotelThumbnailPropertyJSONKeyName,
             @"streetAddress" : kJQHotelStreetAddressPropertyJSONKeyName,
             @"reviewScore" : kJQHotelReviewScorePropertyJSONKeyName
             };
}

@end