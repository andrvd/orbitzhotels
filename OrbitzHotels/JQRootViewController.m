//
//  JQRootViewController.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQRootViewController.h"
#import "JQMainViewController.h"
#import "JQDataManager.h"
#import "JQHotel.h"
#import "JQHotelAdapter.h"

@interface JQRootViewController ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic, strong) NSArray* hotelAdapters;
@end

@implementation JQRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureUI];
    [self start];
}

- (void)configureUI
{
    self.spinner.hidesWhenStopped = YES;
    [self.spinner startAnimating];
}

- (void)start
{
    __weak typeof(self) welf = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [welf loadHotels];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            
            if (welf.hotelAdapters.count > 0) {
                [welf performSegueWithIdentifier:@"showMain" sender:welf];
            } else {
                [welf presentError];
            }
        });
    });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showMain"]) {
        JQMainViewController* mainViewController = segue.destinationViewController;
        mainViewController.hotelAdapters = self.hotelAdapters;
    }
}

#pragma mark - Loading data

- (void)loadHotels
{
    JQDataManager* dataManager = [[JQDataManager alloc] init];
    NSArray* hotels = [dataManager data];
    self.hotelAdapters = [self createAdaptersForHotels:hotels];
}

- (NSArray*)createAdaptersForHotels:(NSArray*)hotels
{
    __block NSMutableArray* mutableArray = [NSMutableArray array];
    [hotels enumerateObjectsUsingBlock:^(JQHotel* hotel, NSUInteger idx, BOOL *stop) {
        JQHotelAdapter* hotelAdapter = [[JQHotelAdapter alloc] initWithHotel:hotel];
        hotelAdapter.index = idx + 1;
        
        NSError* error;
        if (hotelAdapter.thumbnailURL) {
         NSData* data = [NSData dataWithContentsOfURL:hotelAdapter.thumbnailURL
                                             options:NSDataReadingMappedIfSafe
                                               error:&error];
            if (error != nil) {
                *stop = YES;
                return;
            }
            
            UIImage* image = [UIImage imageWithData:data];
            hotelAdapter.thumbnailImage = image;
        }
       
        [mutableArray addObject:hotelAdapter];
    }];
    
    if (mutableArray.count > 0) {
        NSArray* resultArray = [NSArray arrayWithArray:mutableArray];
        return resultArray;
    }
    
    return nil;
}

- (void)presentError
{
    [self.spinner stopAnimating];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:@"Unable to load data. Please check if you have internet connection."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
}
@end
