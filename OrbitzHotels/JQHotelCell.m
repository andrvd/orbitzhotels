//
//  JQHotelCell.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQHotelCell.h"

@interface JQHotelCell()
@property (weak, nonatomic) IBOutlet UILabel *milesLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rateImageView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@end

@implementation JQHotelCell

- (void)setBackgroundImage:(UIImage *)backgroundImage
{
    self.backgroundImageView.image = backgroundImage;
}

- (UIImage *)backgroundImage
{
    return self.backgroundImageView.image;
}

- (void)setRateImage:(UIImage *)rateImage
{
    self.rateImageView.image = rateImage;
}

- (UIImage *)rateImage
{
    return self.rateImageView.image;
}

- (NSString *)miles
{
    return self.milesLabel.text;
}

- (void)setMiles:(NSString *)miles
{
    self.milesLabel.text = miles;
}

- (NSString *)name
{
    return self.nameLabel.text;
}

- (void)setName:(NSString *)name
{
    self.nameLabel.text = name;
}

- (NSString *)address
{
    return self.addressLabel.text;
}

- (void)setAddress:(NSString *)address
{
    self.addressLabel.text = address;
}

- (NSString *)price
{
    return self.priceLabel.text;
}

- (void)setPrice:(NSString *)price
{
    self.priceLabel.text = price;
}

@end
