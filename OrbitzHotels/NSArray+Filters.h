//
//  NSArray+Filters.h
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/29/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Filters)
- (NSArray*)filterByHigestStarRating;
- (NSArray*)filterByLowestStarRating;
- (NSArray*)filterByHigestTotalRate;
- (NSArray*)filterByLowestTotalRate;
- (NSArray*)filterByNearestLocation;
- (NSArray*)filterByFurthestLocation;
- (NSArray*)filterWithQuery:(NSString*)query;
@end
