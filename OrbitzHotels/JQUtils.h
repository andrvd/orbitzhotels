//
//  JQUtils.h
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JQUtils : NSObject
+ (NSDictionary*)hotelsDictionary;
@end
