//
//  NSArray+Filters.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/29/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "NSArray+Filters.h"

@implementation NSArray (Filters)

- (NSArray*)filterByHigestStarRating
{
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"starRatingValue" ascending:NO];
    return [self sortedArrayUsingDescriptors:@[descriptor]];
    
}

- (NSArray*)filterByLowestStarRating
{
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"starRatingValue" ascending:YES];
    return [self sortedArrayUsingDescriptors:@[descriptor]];
    
}

- (NSArray*)filterByLowestTotalRate
{
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"totalRateValue" ascending:YES];
    return [self sortedArrayUsingDescriptors:@[descriptor]];
    
}

- (NSArray*)filterByHigestTotalRate
{
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"totalRateValue" ascending:NO];
    return [self sortedArrayUsingDescriptors:@[descriptor]];
    
}
- (NSArray*)filterByNearestLocation
{
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"distanceValue" ascending:YES];
    return [self sortedArrayUsingDescriptors:@[descriptor]];
    
}

- (NSArray*)filterByFurthestLocation
{
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"distanceValue" ascending:NO];

    return [self sortedArrayUsingDescriptors:@[descriptor]];
    
}

- (NSArray*)filterWithQuery:(NSString*)query
{
    NSPredicate *searchPredicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", query];
    NSArray* filteredArray = [self filteredArrayUsingPredicate:searchPredicate];
    return filteredArray;
}

@end
