//
//  JQUtils.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQUtils.h"

@implementation JQUtils

+ (NSDictionary*)hotelsDictionary
{
    NSBundle* testTargetBundle = [NSBundle mainBundle];
    NSString* filePath = [testTargetBundle pathForResource:@"hotels" ofType:@"json"];
    NSString* jsonString = [NSString stringWithContentsOfFile:filePath encoding:NSStringEncodingConversionExternalRepresentation error:nil];
    NSData* jsonData = [jsonString dataUsingEncoding:NSStringEncodingConversionExternalRepresentation];
    NSError* error;
    NSDictionary* result = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    return result;
}
@end
