//
//  JQHotelCell.h
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JQHotelCell : UITableViewCell
@property (nonatomic, readwrite) UIImage* backgroundImage;
@property (nonatomic, readwrite) UIImage* rateImage;
@property (nonatomic, readwrite) NSString* name;
@property (nonatomic, readwrite) NSString* miles;
@property (nonatomic, readwrite) NSString* address;
@property (nonatomic, readwrite) NSString* price;
@end
