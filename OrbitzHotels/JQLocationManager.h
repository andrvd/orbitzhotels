//
//  JQLocationManager.h
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/29/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface JQLocationManager : NSObject

@property (nonatomic, readwrite, weak) id <CLLocationManagerDelegate> delegate;

+ (id)sharedInstance;
- (void)start;
- (CLLocation*)location;

@end
