//
//  JQSearchBarController.m
//  OrbitzHotels
//
//  Created by Jorge Quezada on 8/28/15.
//  Copyright (c) 2015 Jorge Quezada. All rights reserved.
//

#import "JQSearchBarController.h"
#import "JQHotelCell.h"
#import "NSArray+Filters.h"
#import "JQContainerView.h"

typedef enum {
    NoFilter,
    NearestLocationFilter,
    FurthestLocationFilter,
    HigestTotalRateFilter,
    LowestTotalRateFilter,
    HigestStarRatingFilter,
    LowestStarRatingFilter
} SearchFilter;

@interface JQSearchBarController ()
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (nonatomic, assign) CGSize keyboardSize;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSMutableArray* filteredHotels;
@property (nonatomic) BOOL resultsWereCleared;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterButtonWidth;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (nonatomic, assign) SearchFilter currentFilter;
@end

@implementation JQSearchBarController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureContainers];
    [self observeKeyboardNotifications];
    [self configureTableView];
}

- (void)configureContainers
{
     ((JQContainerView*)self.view).shouldForwardTouches = YES;
}

- (void)configureTableView
{
    [self.tableView registerNib:[UINib nibWithNibName:@"JQHotelCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"JQHotelCell"];
    self.tableView.rowHeight = 91.0f;
    self.tableViewHeight.constant = 0;
    self.currentFilter = HigestStarRatingFilter;
    [self clearResults];
}

#pragma mark - UISearchBarDelegate methods

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self clearResults];
    searchBar.text = @"";
    [searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if (self.resultsWereCleared == NO) {
        [self updateFilterWithResults:[self resultsFiltered] animated:YES];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0) {
        if ([searchBar isFirstResponder]) {
            [self clearResults];
        }
        return;
    }
    
    [self displayFilter:YES];
    self.resultsWereCleared = NO;
    NSArray* filteredArray = [[self resultsFiltered] filterWithQuery:searchText];
    [self updateFilterWithResults:filteredArray animated:YES];
    
}

#pragma mark - Keyboard

- (void)observeKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardDidShow:(NSNotification*)notification
{
    self.keyboardSize = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    self.keyboardSize = CGSizeZero;
}

#pragma mark - UITableViewDelegate methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    JQHotelAdapter* hotel = [self hotelForIndexPath:indexPath];
    if (self.actionBlock) {
        self.actionBlock(hotel);
        [self searchBarCancelButtonClicked:self.searchBar];
    }
    [self clearResults];
}

#pragma mark - UITableViewDataSource methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.filteredHotels.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellID = @"JQHotelCell";
    JQHotelCell* cell = [self.tableView dequeueReusableCellWithIdentifier:cellID];
    JQHotelAdapter* hotel = [self hotelForIndexPath:indexPath];
    cell.name = hotel.name;
    cell.address = hotel.streetAddress;
    cell.miles = hotel.distance;
    cell.price = hotel.totalRate;
    cell.backgroundImage = hotel.thumbnailImage;
    cell.rateImage = hotel.rateImage;
    
    return cell;
}

- (JQHotelAdapter*)hotelForIndexPath:(NSIndexPath*)indexPath
{
    if (indexPath.row < self.filteredHotels.count) {
        JQHotelAdapter* hotel = self.filteredHotels[indexPath.row];
        return hotel;
    }
    return nil;
}

#pragma mark - Helper methods

- (void)displayFilter:(BOOL)display
{
    if (display) {
        self.filterButtonWidth.constant = 50.0f;
    } else {
        self.filterButtonWidth.constant = 0;
    }
    __weak typeof(self) welf = self;
    [UIView animateWithDuration:0.2 animations:^{
        [welf.view layoutSubviews];
    } completion:nil];
}

- (void)clearResults
{
    self.resultsWereCleared = YES;
    [self displayFilter:NO];
    if (self.hotelAdapters.count > 0) {
        [self updateFilterWithResults:@[] animated:NO];
    }
}

- (void)updateFilterWithResults:(NSArray*)results animated:(BOOL)animated;
{
    self.filteredHotels = [NSMutableArray arrayWithArray:results];
    [self.tableView reloadData];
    [self resizeTableViewAnimated:animated];
}

- (void)resizeTableViewAnimated:(BOOL)animated
{
    CGFloat availableSpaceForTableView = self.tableView.superview.frame.size.height - self.tableView.frame.origin.y - self.keyboardSize.height - 8.0f;
    CGFloat newHeight = MIN(availableSpaceForTableView, self.tableView.contentSize.height);
    
    self.tableViewHeight.constant = newHeight;
    
    if (animated) {
        __weak typeof(self) welf = self;
        [UIView animateWithDuration:0.2 animations:^{
            [welf.view layoutSubviews];
        } completion:nil];
    }
}

- (NSArray*)resultsFiltered
{
    if (self.currentFilter == NearestLocationFilter) {
        return [self.hotelAdapters filterByNearestLocation];
    } else if (self.currentFilter == FurthestLocationFilter) {
        return [self.hotelAdapters filterByFurthestLocation];
    } else if (self.currentFilter == HigestTotalRateFilter){
        return [self.hotelAdapters filterByHigestTotalRate];
    } else if (self.currentFilter == LowestTotalRateFilter) {
        return [self.hotelAdapters filterByLowestTotalRate];
    } else if (self.currentFilter == HigestStarRatingFilter) {
        return [self.hotelAdapters filterByHigestStarRating];
    } else if (self.currentFilter == LowestStarRatingFilter) {
        return [self.hotelAdapters filterByLowestStarRating];
    } else {
        return self.hotelAdapters;
    }
}

- (UIAlertAction*)alertActionWithTitle:(NSString*)title filter:(SearchFilter)filter alert:(UIAlertController*)alert
{
    __weak typeof(self) welf = self;
    UIAlertAction* alertAction = [UIAlertAction
                                   actionWithTitle:title
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       welf.currentFilter = filter;
                                       [welf updateFilterWithResults:[welf resultsFiltered] animated:YES];
                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                   }];
    return alertAction;
}

- (void)presentActionSheet
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Action required"
                                  message:@"Please select a filter"
                                  preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* highestRated = [self alertActionWithTitle:@"Highest rated" filter:HigestStarRatingFilter alert:alert];
    UIAlertAction* lowestRated = [self alertActionWithTitle:@"Lowest rated" filter:LowestStarRatingFilter alert:alert];
    UIAlertAction* higestPrice = [self alertActionWithTitle:@"Highest price" filter:HigestTotalRateFilter alert:alert];
    UIAlertAction* lowestPrice = [self alertActionWithTitle:@"Lowest price" filter:LowestTotalRateFilter alert:alert];
    UIAlertAction* nearestLocation = [self alertActionWithTitle:@"Nearest location" filter:NearestLocationFilter alert:alert];
    UIAlertAction* futhestLocation = [self alertActionWithTitle:@"Furthest location" filter:FurthestLocationFilter alert:alert];
    UIAlertAction* cancel = [self alertActionWithTitle:@"Cancel" filter:NoFilter alert:alert];

    [alert addAction:highestRated];
    [alert addAction:lowestRated];
    [alert addAction:higestPrice];
    [alert addAction:lowestPrice];
    [alert addAction:nearestLocation];
    [alert addAction:futhestLocation];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Actions

- (IBAction)filterButtonTapped:(id)sender {
    [self presentActionSheet];
}
@end

